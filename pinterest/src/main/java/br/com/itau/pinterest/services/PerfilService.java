
package br.com.itau.pinterest.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.itau.helper.JwtHelper;
import br.com.itau.pinterest.api.PerfilEntrada;
import br.com.itau.pinterest.models.Login;
import br.com.itau.pinterest.models.Perfil;
import br.com.itau.pinterest.repositories.LoginRepository;
import br.com.itau.pinterest.repositories.PerfilRepository;

@Service
public class PerfilService {
	@Autowired
	PerfilRepository perfilRepository;
	@Autowired
	LoginRepository loginRepository;

	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public void inserir(PerfilEntrada perfilEntrada) {
		inserirPerfil(perfilEntrada);
		inserirLogin(perfilEntrada);
	}

	private void inserirPerfil(PerfilEntrada perfilEntrada) {
		Perfil perfil = new Perfil();
		perfil.setNome(perfilEntrada.getNome());
		perfil.setEmail(perfilEntrada.getEmail());
		perfil.setSexo(perfilEntrada.getSexo());
		perfil.setDataNAscimento(perfilEntrada.getDataNascimento());
		perfil.setPreferencias(perfilEntrada.getPreferencias());
		perfilRepository.save(perfil);
	}

	private void inserirLogin(PerfilEntrada perfilEntrada) {
		Login login = new Login();

		String hash = encoder.encode(perfilEntrada.getSenha());

		login.setEmail(perfilEntrada.getEmail());
		login.setSenha(hash);
		loginRepository.save(login);
	}

	public Optional<String> fazerLogin(Login credenciais) {
		Optional<Login> loginOptional = loginRepository.findById(credenciais.getEmail());

		if (!loginOptional.isPresent()) {
			if (encoder.matches(credenciais.getSenha(), loginOptional.get().getSenha())) {
				return JwtHelper.gerar(credenciais.getEmail());

			}
		}
		return Optional.empty();
	}
}