
package br.com.itau.pinterest.repositories;

import org.springframework.data.repository.CrudRepository;
import br.com.itau.pinterest.models.Login;

public interface LoginRepository extends CrudRepository<Login, String> {
}
