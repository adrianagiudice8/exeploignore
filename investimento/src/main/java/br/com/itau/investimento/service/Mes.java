package br.com.itau.investimento.service;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

public class Mes {

	Locale locale = new Locale("pt", "BR");
	{

		for (Month mes : Month.values()) {
			System.out.println(mes.getDisplayName(TextStyle.FULL, locale));
		}
	}

	
}
