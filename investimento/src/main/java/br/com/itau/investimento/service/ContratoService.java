package br.com.itau.investimento.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.itau.investimento.Investimento;
import br.com.itau.investimento.controllers.Controller;

@Service
public class ContratoService {
	private static List<Investimento> investimentos;
	private static int idAtual;

	public ContratoService() {
		investimentos = new ArrayList<>();
		idAtual = 1;

		Investimento investimento1 = new Investimento();
		investimento1.setNome("Poupança");
		double valortaxa1 = investimento1.getValor() * 0.05;
		investimento1.setValor(valortaxa1); 
		inserirInvestimento(investimento1);

	}
	
	public double Calcular(double valor) {
		double valortaxa1 = valor * 0.05;
		return valor;
			}

	private Investimento obterInvestimento(int id) {
		for (Investimento investimento : investimentos) {
			if (investimento.getId() == id) {
				return investimento;
			}
		}
		return null;
	}

	public static List<Investimento> getInvestimento() {
		return investimentos;
	
	}

	public static boolean inserirInvestimento(Investimento investimento ) {
		investimento.setId(idAtual);
		idAtual++;
		return investimentos.add(investimento);
	}
}
