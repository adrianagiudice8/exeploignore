package br.com.itau.produto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.produto.models.Cliente;
import br.com.itau.produto.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	@Autowired
	ClienteService clienteService;

	@PostMapping
	public ResponseEntity criar(@RequestBody Cliente cliente){
		cliente = clienteService.cadastrar(cliente);
		
		return ResponseEntity.status(201).body(cliente);
	}
	
	@GetMapping("cpf")
	public ResponseEntity buscar(@RequestBody String cpf){
		Optional<Cliente> clienteOptional = clienteService.buscar(cpf);
		
		if(clienteOptional.isPresent())
		{
			return ResponseEntity.ok(clienteOptional.get()));
		}
		
		return ResponseEntity.notFound().build();
			
					cliente = clienteService.cadastrar(cliente);
		}
		
		return ResponseEntity.status(201).body(cliente);
}
