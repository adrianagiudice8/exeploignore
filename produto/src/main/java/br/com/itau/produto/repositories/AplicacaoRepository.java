package br.com.itau.produto.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.produto.models.Aplicacao;

public interface AplicacaoRepository extends CrudRepository<Aplicacao, Integer> {

}
