package br.com.itau.produto.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.produto.models.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

}
